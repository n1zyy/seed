+++
date = 2019-08-21T13:53:34Z
title = "Ansible"
[taxonomies]
  audience = [ "admin" ]
  topic = ["ansible"]
+++

[Ansible](https://www.ansible.com/) is the server configuration automation tool
we use to ensure our servers are configured in a uniform way.

Our ansible setup involves to git repositories:

 * seed is the name of the git repository containing our roles and most of our
   ansible code. It also includes the inventories/testing directory, which
   allows you to create a set of docker images that replicates the full
   functionality of our server system and provides a hosts.yml file that
   references them, allowing for a completely self-contained eco-system that
   mimics our full infrastructure.

 * in addition, we have a private repository containing the production inventory.

For more information, see:

 * [hacking ansible](hacking-ansible) - instructions for checking out and hacking on our ansible code.
 * [ansible vault](ansible-vault) - how we store secrets in ansible
