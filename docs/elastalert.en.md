+++
date = 2019-08-13T13:53:34Z
title = "Elasticsearch setup"
[taxonomies]
  audience = [ "admin" ]
  topic = ["logging"]
+++

[Elastalert](https://github.com/Yelp/elastalert) combined with
[monitor](monitor) and [elasticsearch](elasticsearch) provides monitoring
services, which includes both sending alerts and also maintaining a state (so
we can tell which alerts have not been resolved).

Elastalert handles the alerting part natively, however, it has no method for
maintaing state.

So, we have implemented a handful of alert commands to maintain state and
generate an html file displaying that state.

 * `mf-track-alert` is called whenever elastalert finds a SENDALERT report.
   This script creates a file in /var/run/mayfirst/alerts/<host>/<service> with
   the content of the alert as the content of the file. It sets the
   modification date of the file to the date at which it should expire (each
   monitor alert comes with an expiration setting such as 10m, 1h, 1d, or 1w).
 * `mf-clear-alert` is called whenever elastalert finds a ALLGOOD report. If
   there is an entry for the host and service in /var/run/mayfirst/alerts then
   it is deleted.
 * `mf-expire-alert` searches for alerts older than 1 minute (which means they
   should be expired) and it deletes them.
 * `mf-alerts-to-html` finds all alerts in /var/run/mayfirst/alerts and
   generates an index.html file listing them.

## Rules

Elastalert has [good
documentation](https://elastalert.readthedocs.io/en/latest/) which explains how
rules and alerts are created.

 * rule: rules are queries against elasticsearch that either return a match or don't
 * alerts: alerts are what should happen if a rule returns a match.
 
While [es](elasticsearch) holds all our critical data and should be housed in a
protected environment, elastalert can run on a remote server and query es over
the Internet so it can more effectively alert us to problems.
