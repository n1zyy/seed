+++
date = 2019-08-13T13:53:34Z
title = "Elasticsearch setup"
[taxonomies]
  audience = [ "admin" ]
  topic = ["logging"]
+++

[Elasticsearch](https://www.elastic.co/products/elasticsearch) (es) is
installed on `log-xxx` servers. It's a database designed to injest large amounts of
data while providing an easy to use query language to search it.

It's installed using es provided apt repos (not in Debian).

The `log-xxx` server(s) are dedicated to just *running* Elasticsearch. Other
servers and services work together with Elasticsearch, including:

 * [Journalbeat](journalbeat) - all servers run
   [Journalbeat](https://www.elastic.co/guide/en/beats/journalbeat/current/index.html),
   which send journald data to the es server.
 * [Kibana](kibana) - runs on `monitor-xxx` and provides a user interface for
   querying the es server. It's also availabley via the simpler `monitor.{{
   Site.DomainName  }}:5601` URL.
 * [Elastalert](elastalert) - also runs on `monitor-xxx` and tails the data going
   into es and provides alerts when pre-defined conditions are met.

## User management

We use basic auth with pre-defined usernames and passwords to grant access to
the various programs that send and/or monitor our es server, including
`kibana`, `journalbeat_writer`, and `elastalert` users.

Their passwords are set in a [ansible vault](ansible-vault) file called
secrets.yml kept in the inventory.
