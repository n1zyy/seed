+++
date = 2019-08-13T13:53:34Z
title = "Monitor scripts"
[taxonomies]
  audience = [ "admin" ]
  topic = ["logging"]
+++

We use a simple library of shell scripts to monitor various states on our
servers and report to journald if we find any problems (or we report ALLGOOD if
there are no problems).

All journald data is in turn centralized via [journalbeat](journalbeat) on our
`log-xxx` server which is running [elasticesearch](elasticsearch).

The parent script (`mf-monitor`) checks a configuration file
(/usr/local/etc/mayfirst/monitor.conf) to find out which tests to run. All
tests are simple shell scripts that either have no output (to indicate all is
fine) or they output a succinct message explaining what the problem is.

The `mf-monitor` is run via a systmed-timer.

Each test is configured to run every 10 minutes, once an hour, once a day or
once a week.
