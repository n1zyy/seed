# Documentation

Until we have a better plan, place markdown formatted files in this directory
as stubs for full documentation, both user-facing and admin-facing.

The goal is to try to document as we go in a generic enough format that it can
be incorporated anywhere.

Here are some initial guidelines.

## Audience and naming

Write separate files for users and admins. 

Use the shortest, simplest name for the file.

For users, pick a name that describes the help file in their terms, e.g.
webmail.en.md might describe all webmail options instead of
roundcube.en.md. Our software may change, but we want the webmail page to
remain the same.

For admins, do the opposite, e.g. roundcube.en.md and horde.en.md will
describe how these programs are setup and configured.

Other examples: web-conference.en.md for users and jitsi-meet.en.md for the
admin page; share.en.md for users and nextcloud.en.md for admins;
jabber.en.md for users and prosody.en.md for admins. 

## Language

All english files should end with en.md, spanish files with es.md. Files don't
have to be translated into Spanish as we go.

## Taxonomy

Let's start with two simple taxonomies:

 * audience
   * admin
   * user

 * topic (add terms as necessary)
   * email
   * email lists
   * web
   * dns
   * database
   * ansible

## hugo front matter

Please use simple hugo style front matter:

+++
date = 2015-10-01T13:53:34Z
title = "Set up your database with Drupal"
[taxonomies]
  audience = [ "user" ]
  topic = ["web", "database"]
+++

## Conventions for referencing servers and URLs

The goal is for the documentation to be useful both to help while in dev mode
and also to be used, as is, when we go live.

That makes it hard to refer to URLs and server names. For example, in dev mode,
the default URL is: mayfirst.test. But in live mode, it will be mayfirst.org.
So, instead of writing those URLs, let's write the variable: `{{
.Site.DomainName }}`, e.g. "Connect to kibana via: https://monitor.{{ .Site.DomaniName }}:5601/"

Also, in our dev environment, all servers are named sequentially for simplicity
(e.g. nscache-001, nscache-002), however, in production mode we will want to
choose randomized names (to help protects us from DDOS attacks). So, when
referring to a particular server, let's write nscache-xxx to mean whichever
nscache server is relevant, e.g. "SSH into root@nscache-xxx.{{ .Site.DomainName
}}".
