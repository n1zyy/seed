+++
date = 2019-08-21T13:53:34Z
title = "Hacking Ansible"
[taxonomies]
  audience = [ "admin" ]
  topic = ["ansible"]
+++

[Ansible](ansible) is the server configuration automation tool
we use to ensure our servers are configured in a uniform way.

If you want to hack on our code, start by checking out the repository:

    git clone git://git.mayfirst.org:mfpl/seed.git

## Preparing your host

Our playbook has been developed on a Debian system (buster).

Minimally, you will need:

    apt install docker ansible

In addition, `elasticsearch` will not run properly unless the `max_map_count`
is at least 262144. Since it's a kernel setting it has to be set on your host.

    sudo sysctl -w vm.max_map_count=262144

That temporarily sets it (but won't retain on reboot). If you want it to survive a reboot, add a file in /etc/sysctl.d/local.conf with:

    vm.max_map_count=262144

## Getting started

To get started on your own development machine, you will need to take a few steps:

 * Create a series of docker containers representing all the server groups we use
 * Create your own certificate authority

The directions for how to take each of these steps is in the README files in
the inventories/testing/docker and inventories/testing/ca directories.

## Orientation

The git repo is layed out like a typical ansible playbook. The main directories of interest are:

 * inventories - see testing/hosts.yml - that is the list of hosts we will test
 * roles - this directory contains all the playbooks

## Ready?

If you are ready, you run the command to apply all of our roles with:

    ansible-playbook -i inventories/testing/hosts.yml -e @inventories/testing/secrets.yml --vault-password-file inventories/testing/secret.txt site.yml

## Tips

If you use the monkeysphere, you may want to disable it for the test servers by
adding the following to your ~/.ssh/config file:

    Host *.mayfirst.test
    ProxyCommand none

## Adding roles

To initialize an empty role directory:

cd roles/admin
ansible-galaxy init rolename
