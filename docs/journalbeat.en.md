+++
date = 2019-08-13T13:53:34Z
title = "Journalbeat"
[taxonomies]
  audience = [ "admin" ]
  topic = ["logging"]
+++

[Journalbeat](https://www.elastic.co/guide/en/beats/journalbeat/current/index.html)
sends all journald logging to our [elasticsearch](elasticsearch) server.

All servers (by default, not a requirement) run journalbeat to their logs can
be analzed in aggregate. All servers share a journalbeat username and password
authorizing them to connect to the es server.

Journalbeat doesn't seem to work properly on /var/run/systemd/journalbeat (more
testing necessary) so creating /var/log/journal seems to be required (which
means more disk i/o)
