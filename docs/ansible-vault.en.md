+++
date = 2019-08-21T13:53:34Z
title = "Ansible vault"
[taxonomies]
  audience = [ "admin" ]
  topic = ["ansible"]
+++

[Ansible](ansible) provides a mechanism for storing secrets in a dedicated and
encrypted file. It's not ideal to have to keep secrets at all, but it some
cases it seems worth the risks, particularly in order to have continuous
integration working.

The ansible vault file should be placed in the inventory directory, e.g.:
    
    inventories/testing/secrets.yml

To ease continuous intergration, for the testing inventory, we have a
secret.txt file that holds the password to unlock the secrets.yml vault (the
password is: secret).

You can run our playbooks with the vault passwords available by passing:

    -e @inventories/testing/secrets.yml --vault-password-file inventories/testing/secret.txt

when executing the ansible-playbook command.

Or, you can skip it and the passwords will not be available.

The roles *should* be designed to only need the passwords on the first run. On
subsequent runs, if the passwords are not available, the tasks that set
passwords should be skipped.

Useful ansible-vault commands include:

    ansible-vault view inventories/testing/secrets.yml
    ansible-vault edit inventories/testing/secrets.yml

