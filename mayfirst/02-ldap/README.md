# LDAP basics

This directory provides some help learning ldap.

By default, the admin user will have a dn of: "cn=admin, dc=mayfirst, dc=test"
and a password of: admin 

Create a new user by modifying the first-user.ldiff file. The userPassword
field demonstrates how you can take an already encrypted password and shove
into ldap (you just have to preface it with {crypt}). For the record, that
sha512 encrypted password is: joe.

Insert the new record with:

    ldapadd -p 9389 -h a.ldap.mayfirst.test -D "cn=admin, dc=mayfirst, dc=test" -W -f first-user.ldiff 

Note: -D sets the username to bind with, -W means to prompt for a password , -p
sets the port, -h sets the host, and -f specifies the file to add.

Search for the record we just added:

    ldapsearch -p 9389 -h a.ldap.mayfirst.test -b "dc=mayfirst, dc=test" -D "cn=admin, dc=mayfirst, dc=test" -W "(uid=jamie)"

The -b sets the base directory for the search.

Now, let's set a new password: 

    ldappasswd -p 9389 -h a.ldap.mayfirst.test -S -W -D "cn=admin,dc=mayfirst,dc=test" -x "uid=jamie, dc=mayfirst, dc=test"

Check to make sure we can login with that password:

    ldapwhoami -p 9389 -h a.ldap.mayfirst.test -D "uid=jamie, dc=mayfirst, dc=test" -W

And, we can delete the record:

    ldapdelete -p 9389 -h a.ldap.mayfirst.test -D "cn=admin, dc=mayfirst, dc=test" -W "uid=jamie, dc=mayfirst, dc=test"
