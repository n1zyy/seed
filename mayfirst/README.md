# May First ansible/ldap setup

This ansible playbook is designed to help us learn ldap better and test using
one ldap directory with many different services.

Aside from installing ansible and ldap-utils, some basic DNS configuration is
necessary on your host.

The domain name used throughout is: mayfirst.test

And, the IP range used is 127.0.99.0 - 127.0.99.255

And, all ports have 9000 added to them (e.g. port 80 becomes port 9080, etc).

So, minimally, you will need to create an entry in /etc/hosts for:

 * a.ldap.mayfirst.test: 127.0.99.10

