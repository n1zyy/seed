# Creating a certificate authority

You can't really build services without encryption, and you can't have
encryption without tls certificates.

However, tls certificates make testing really hard. 

When running in production, we will use Lets Encrypt to generate and verify our
certificates and we know that all of our tools trust Lets Encrypt certificates.

But that doesn't work when testing, since Lets Encrypt won't validate our test
certificates.

That where this help file comes in. 

For testing our infrastructure, we will create our own certificate authority
and then we will add our certificate authority to all of our test nodes so it
is trusted.

If you run the `setup-ca` script all will be taken care of for you, and the
ansible playbooks will use the generated files.

If you want to test web connections to any of the services created in your dev
environment, then you will need to import the seed-rootCA.pem file into the
Certificate Manager Authorities section of your web browser.
