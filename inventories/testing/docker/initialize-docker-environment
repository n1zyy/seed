#!/bin/bash

# Create base images and default containers. Pass force as the first argument
# to force a rebuild of all images and containers.

base_net=172.18.0
domain=mayfirst.test
hosts_file_additions=""
apt_server=http.us.debian.org/debian/

if [ -n "$SEED_APT_PROXY" ]; then
  apt_proxy="$SEED_APT_PROXY"
  if [ "${apt_proxy: -1}" != "/" ]; then
    apt_proxy="${apt_proxy}/"
  fi
  apt_server="${apt_proxy}${apt_server}"
else
  # We need to add http to the apt_server if we are not using a apt_proxy
  apt_server="http://${apt_server}"
fi

force=0
if [ "$1" == "force" ]; then
  printf "Forcing rebuild.\n"
  force=1
fi

build_seed_network() {
  if network_exists seed; then
    printf "The seed network already exists.\n"
    return
  fi

  # We should pass --ipv6 but that seems broken:
  # https://github.com/moby/moby/issues/28055
  docker network create --subnet=${base_net}.0/16 seed
}

network_exists() {
  if docker network ls | grep " $1 " >/dev/null; then
    return 0
  fi
  return 1
}

build_my_buster_image() {
  if image_exists my-buster; then
    printf "The my-buster image is already created.\n"
    return
  fi

  printf "Building my-buster image\n"
  user=$(whoami)
  sudo=""
  if [ "$user" != "root" ]; then
    printf "You have to be root to create the first base image\n"
    exit 1
  fi

  temp=$(mktemp -d)

  # Ensure we don't create a 700 permission root file system in our container.
  chmod 755 "$temp"

  echo "Running debootstrap"
  export LC_ALL=C && debootstrap --variant=minbase --include=apt-utils,less,iputils-ping,iproute2,vim,locales,libterm-readline-gnu-perl,dnsutils,procps buster "$temp" "${apt_server}" 

  # Make all servers America/New_York
  echo "America/New_York" > "$temp/etc/timezone"
  chroot "$temp" /usr/sbin/dpkg-reconfigure --frontend noninteractive tzdata

  # Upgrade
  echo "deb http://security.debian.org/ buster/updates main" > "$temp/etc/apt/sources.list.d/security.list"
  echo "deb http://ftp.us.debian.org/debian/ buster-updates main" > "$temp/etc/apt/sources.list.d/update.list"
  if [ -n "$apt_proxy" ]; then
    echo "Acquire::http::proxy \"$apt_proxy\";" > "$temp/etc/apt/apt.conf.d/02proxy"
  fi
  echo "Upgrading"
  chroot "$temp" apt-get update
  chroot "$temp" apt-get -y dist-upgrade

  echo "Importing into docker"
  cwd=$(pwd)
  cd "$temp" && tar -c . | docker import - my-buster
  cd "$cwd"
  echo "Removing temp directory"
  rm -rf "$temp"
}

build_seed_base_image() {
  if [ "$force" -ne "1" ] && image_exists "seed-base"; then
    printf "The seed-base image already exists.\n"
    return
  fi

  printf "Building seed-base image\n"
  docker build -t seed-base ./
}

image_exists() {
  if docker images "$1" | grep "$1" >/dev/null; then
    return 0
  fi
  return 1 
}

build_container() {
  name="$1"

  if [ "$force" -eq 1 ]; then
    if container_is_running "$name"; then
      docker stop "$name"
    fi

    if container_is_built "$name"; then
      docker rm "$name"
    fi
  fi

  if container_is_built "$name"; then
    printf "The %s container is already built.\n" "$name"
    return
  fi

  printf "Building %s container.\n" "$name"

  ip="$2"
  
  # Note: with buster, the --cap-add command is no longer needed because newer
  # systemd in buster can deal without it. 

  docker create --net seed --ip "$ip"  -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    -v "$(pwd)/resolv.conf:/etc/resolv.conf" --hostname "$name" --name "$name" \
    seed-base
}

start_container() {
  name="$1"
  if container_is_running "$name"; then
    printf "The %s container is already running.\n" "$name"
    return
  fi

  printf "Starting %s\n" "$name"
  docker start "$1"
}

container_is_running() {
  if docker ps | grep "$1" >/dev/null; then
    return 0
  fi
  return 1
}

container_is_built() {
  if docker ps -a | grep "$1\$" >/dev/null; then
    return 0
  fi
  return 1
}

build_and_start_containers() {
  ip_fragment=10
  for container in $1; do
    ip=${base_net}.${ip_fragment}
    build_container "$container" "$ip"
    ip_fragment=$(( $ip_fragment + 1 ))
    start_container "$container"
    if ! grep "^${ip} ${container}\.${domain}\$" /etc/hosts >/dev/null; then
      hosts_file_additions="$hosts_file_additions
      ${ip} ${container}.${domain}"
    fi
    # For the monitor container, we also want monitor.mayfirst.test in the hosts
    # file so we can access kibana from our web browser.
    if [ "$container" == "monitor-001" ]; then
      if ! grep "^${ip} monitor\.${domain}\$" /etc/hosts >/dev/null; then
        hosts_file_additions="$hosts_file_additions
        ${ip} monitor.${domain}"
      fi
    fi
  done
}

# Main program logic.
build_my_buster_image
build_seed_base_image
build_seed_network

containers="ldap-001 nsauth-001 nscache-001 shell-001 webstore-001 webproxy-001 mailstore-001 mailcf-001 mailmx-001 mailrelay-001 test-001 monitor-001 log-001"
build_and_start_containers "$containers" 

if [ -n "$hosts_file_additions" ]; then
  printf "Add to your host file: %s\n" "$hosts_file_additions"
fi
