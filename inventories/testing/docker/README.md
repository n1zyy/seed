# Initialize your docker environment.

## Overview

The docker images are used for testing and learning purposes only.

The `initialize-docker-environment` script builds out a set of docker
containers that are references in the inventories/testing/hosts file.

The script builds out docker images for convenience. It builds them in 
non-standard ways. In particular:

 * It builds our own base debian jessie image so we don't have to blindly pull
   images from the Internet and run them on our machines.
 * It builds out docker images that run systemd on the inside. Most docker
   images either don't run an init program (they run the service directly) or
   they use a simpler init system (like dumb-init). These images are build
   using systemd so they more closely reflect the way our actual guests are
   run.
 * It install openssh in the image without re-creating the server keys. This
   means the private keys are embedded in the image. This is a big security
   problem for production sites, but a nice convenience for testing because it
   means you don't have to keep approving ssh keys every time you re-create
   containers.

## Steps

You will need to install a relatively modern version of docker (version 18 or
above). If you are running buster, the default debian version will do fine.

You also need to copy your public ssh key to a file, in this directory, called
id_rsa.pub.

Next, run the `initialize-docker-environment` command as root. This script will:

 * Create a debian jessie docker image
 * Create a seed-base image.
 * Build out all containers used in the testing ansible environment.

You can rebuild everything at anytime by passing the "force" argument to 
`initialize-docker-environment`.

## Using an APT proxy.

If you are using an apt proxy, run the command with:

    SEED_APT_PROXY=http://blah.proxy.org/debian ./initialize-docker-environment
