import unittest
import greenhouse 
from time import sleep

class GreenhouseTestCase(unittest.TestCase):
	planter = None

	def setUp(self):
		self.plant = greenhouse.plant()

	def test_connect(self):
		self.plant.ldapLogin()
		self.assertTrue(self.plant.ldapAddUser(), "Add ldap record")
		self.assertTrue(self.plant.ldapAddEmailDomain(), "Add ldap email domain record")
		self.assertTrue(self.plant.emailSend(), "Send email")
		sleep(1)
		self.assertTrue(self.plant.emailReceive(), "Imap login")
		self.assertTrue(self.plant.webTest(), "Web server test")

