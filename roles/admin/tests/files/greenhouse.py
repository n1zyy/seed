# A library of functions to be tested by the test.py script.
import ldap3
import smtplib
import imaplib
import random, string
import urllib.request
import urllib.error

class plant():
  l = None
  server = 'ldap://ldap-001.mayfirst.test'
  passwordFile = "/root/.slapdpassword"
  base = "dc=mayfirst, dc=test"
  adminUser = "cn=admin, dc=mayfirst, dc=test"
  message = None

  def ldapLogin(self):
    server = ldap3.Server(self.server)
    with open(self.passwordFile) as f:
      adminPassword = f.read()
      self.l = ldap3.Connection(server, user = self.adminUser, password = adminPassword, authentication = ldap3.SIMPLE)
      self.l.bind()

  def ldapSearch(self, field, value, ou = None):
    filterstr = "(" + field + "=" + value + ")"
    base = self.base
    if ou:
      base = "ou=" + ou + "," + base
       
    self.l.search(search_base = base, search_scope = ldap3.SUBTREE, search_filter = filterstr)
  
  def ldapAddUser(self):
    while 1:
      self.ldapSearch("uid", "testuser", ou = "users")
      if len(self.l.response) == 1:
        return True
      elif len(self.l.response) == 0:
        dn = "uid=testuser, ou=users, dc=mayfirst, dc=test"
        objectClass = [ "inetOrgPerson", "PostfixAccount" ]
        # The crypted password is: joe
        attributes = {
          "uid": "testuser",
          "givenName": "Test",
          "sn": "McUser",
          "cn": "Test McUser",
          "userPassword": '{crypt}$6$u6ilQEoH$ImUdDm7BUGAgc1bRLrDCTJSp3tCIfbARg4ODNybQvj2viy6K0It8/coFowSIBShG4xKC3C9wS7v7E.9FTnoua.',
          "mail": "tester@tester.test",
          "mailBox": "testuser@mailstore-001.mayfirst.test",
          "mailLocation": "mailstore-001.mayfirst.test",
        }
        self.l.add(dn, object_class = objectClass, attributes = attributes)
      else:
        return False

  def ldapAddEmailDomain(self):
    while 1:
      self.ldapSearch("dc", "tester.test", ou = "emailDomains")
      if len(self.l.response) == 1:
        return True
      elif len(self.l.response) == 0:
        dn = "dc=tester.test, ou=emailDomains, dc=mayfirst, dc=test"
        objectClass = "domain"
        attributes = {
          "dc": "tester.test"
        }
        self.l.add(dn, object_class = objectClass, attributes = attributes)
      else:
        return False
 
  def emailSend(self):
    smtp = smtplib.SMTP("mailcf-001.mayfirst.test", 587)
    letters = string.ascii_lowercase
    self.message = ''.join(random.choice(letters) for i in range(20))
    try:
      smtp.ehlo()
      smtp.starttls()
      smtp.ehlo()
      smtp.login("testuser", "joe")
      smtp.sendmail("grandma@applepie.com", "tester@tester.test", "Subject: {0}\n\nBody".format(self.message))
    finally:
      smtp.quit()
    return True

  def emailReceive(self):
    m = imaplib.IMAP4('mailcf-001.mayfirst.test')
    m.starttls()
    m.login('testuser', 'joe')
    selected = m.select()
    if selected[0] == 'NO':
        print("Failed to select the INBOX via IMAP")
        print(selected)
        return False
    typ, data = m.search(None, 'Subject', self.message)
    found = False
    for num in data[0].split():
      found = True
    m.close()
    m.logout()
    return found 

  def webTest(self):
    try:
      urllib.request.urlopen('http://webstore-001.mayfirst.test')
    except urllib.error.URLError:
      print("Bad URL")
      return False
    return True

  def phpTest(self):
    try:
      urllib.request.urlopen('http://webstore-001.mayfirst.test/helloworld.php')
    except urllib.error.HTTPError:
      print("Not found")
      return False
    return True
