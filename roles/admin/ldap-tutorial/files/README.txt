This file provides some help learning ldap.

Like with MySQL, when slapd is installed by ansible, ansible generates a
file in /root/.slapdpassword that contains the admin password. This file
is saved on ldap-001 and on this server.

By default, the admin user will have a dn of: "cn=admin, dc=mayfirst, dc=test"
and the password in the copied /root/.slapdpassword file. 

Before we can create a new user, we have to create a container to hold users.
Our base is dc=mayfirst, dc=test, so that is already created and available. We
are going to put all users in a container called ou=users, dc=mayfirst, dc=test
and that is not yet created.

NOTE: the ansible scripts take care of this step! So you don't have to
do it. But if you did have to do it, it would look like this:

    ldapadd -h ldap-001.mayfirst.test -D "cn=admin, dc=mayfirst, dc=test" -y /root/.slapdpassword -f add-ou-users.ldiff

Now, we can create a new user. The userPassword field demonstrates how you can
take an already encrypted password and shove into ldap (you just have to
preface it with {crypt}). For the record, that sha512 encrypted password is:
joe.

Insert the new record with:

    ldapadd -h ldap-001.mayfirst.test -D "cn=admin, dc=mayfirst, dc=test" -y /root/.slapdpassword -f add-user.ldiff 

Note: -h sets the host, ,-D sets the username to bind with, -y means get
the password from thye provided path (use -W instead if you want to be
prompted for the password), and -f specifies the file to add.

Search for the record we just added:

    ldapsearch -h ldap-001.mayfirst.test  -D "cn=admin, dc=mayfirst, dc=test" -y /root/.slapdpassword -b "dc=mayfirst, dc=test" "(uid=paul)"

The -b sets the base directory for the search.

Now, let's set a new password: 

    ldappasswd -h ldap-001.mayfirst.test -D "cn=admin,dc=mayfirst,dc=test" -y /root/.slapdpassword -S -x "uid=paul, ou=users, dc=mayfirst, dc=test"

The -S means prompt for the new password, the -x means use simple
authentication instead of SASL.

Check to make sure we can login with that password:

    ldapwhoami -h ldap-001.mayfirst.test -D "uid=paul, ou=users, dc=mayfirst, dc=test" -W

Now we use -W to prompt for the password we just changed.

Now, suppose we want this user to be able to accept email. We have to
add a new class called PosixAccount and add the user's virtual email
address (what people will use to send them email and
their mailbox (our internal representation of where the email should be
delivered).

    ldapmodify -h ldap-001.mayfirst.test -D "cn=admin, dc=mayfirst, dc=test" -y /root/.slapdpassword -f convert-user-to-email-account.ldiff 

They might also want to be able to ssh into the shell server, so they
need to be converted to a posixAccount:

    ldapmodify -h ldap-001.mayfirst.test -D "cn=admin, dc=mayfirst, dc=test" -y /root/.slapdpassword -f convert-user-to-ssh-account.ldiff 

And, if they are going to accept email at the domain myorg.test, we have to add
myorg.test as an acceptable email domain.  

Before we can add the domain, though, we have to create a second container. All
email domains will be added to the container: ou=emailDomains, dc=mayfirst, dc=test:

    ldapadd -h ldap-001.mayfirst.test -D "cn=admin, dc=mayfirst, dc=test" -y /root/.slapdpassword -f add-ou-email-domains.ldiff

Now, we can add the domain myorg.test:

    ldapadd -h ldap-001.mayfirst.test -D "cn=admin, dc=mayfirst, dc=test" -y /root/.slapdpassword -f add-email-domain.ldiff

Lastly, for completeness, this is how to delete a record:

    ldapdelete -h ldap-001.mayfirst.test -D "cn=admin, dc=mayfirst, dc=test" -y /root/.slapdpassword "uid=paul, ou=users, dc=mayfirst, dc=test"
